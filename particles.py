import random
import numpy as np
import cv2
# CONSTANTS
WINDOW_X_SIZE = 1280
WINDOW_Y_SIZE = 720
FRAMERATE = 40

PARTICLE_RADIUS = 5
SPAWN_RATE = 1
EMITTER_X = 400
EMITTER_Y = 300

GRAVITY_X = 0
GRAVITY_Y = 0
VELOCITY_X_BASE = 0
VELOCITY_X_VARIANCE = 0
VELOCITY_Y_BASE = 0
VELOCITY_Y_VARIANCE = 0


# Particle Class Definition
class Particle:
    def __init__(self, xPos, yPos, xVel, yVel):
        self.xPosition = xPos
        self.yPosition = yPos
        self.xVelocity = xVel
        self.yVelocity = yVel
        self.size = 1
        self.thickness = 1
        self.thickness_reverse = 0
        self.color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

        self.dead = False

    def advance(self):
        self.size += 10
        if self.thickness_reverse == 0:
            self.thickness += 1
        if self.thickness_reverse == 1:
            self.thickness -= 1

        if self.thickness == 1:
            self.thickness_reverse = 0
        if self.thickness == 4:
            self.thickness_reverse = 1


        if self.size > 600:
            self.dead = True
        self.xPosition += self.xVelocity
        self.yPosition += self.yVelocity

        if (self.xPosition < 0 or self.xPosition > WINDOW_X_SIZE):
            self.dead = True

        if (self.yPosition < 0 or self.yPosition > WINDOW_Y_SIZE):
            self.dead = True

    def addVelocity(self, xAccl, yAccl):
        self.xVelocity += xAccl
        self.yVelocity += yAccl

    def isDead(self):
        return self.dead

    def draw(self, image, color):
        image = cv2.circle(image, (int(round(self.xPosition)), int(round(self.yPosition))), int((self.size*PARTICLE_RADIUS)/2), self.color, self.thickness)
        return image



def main():
    particlesList = []
    # setInterval( def() { runMainLogic( particlesList ) }, (1000.0 / FRAMERATE) )


def runMainLogic(particlesList, color):
    #spawnLoop(particlesList)
    advanceLoop(particlesList)
    particle_img = renderLoop(particlesList, color)
    return particle_img


def spawnLoop(particlesList):
    for i in range(0, SPAWN_RATE):
        offsetX = (random.randint(0,10) * VELOCITY_X_VARIANCE) - (VELOCITY_X_VARIANCE / 2)
        offsetY = (random.randint(0,10) * VELOCITY_Y_VARIANCE) - (VELOCITY_Y_VARIANCE / 2)
        #particlesList.append(Particle(EMITTER_X, EMITTER_Y, VELOCITY_X_BASE + offsetX, VELOCITY_Y_BASE + offsetY))
        particlesList.append(Particle(EMITTER_X, EMITTER_Y, 0 + 0, 0 + 0))


def advanceLoop(particlesList):
    for i in reversed(range(0, len(particlesList))):
        currentParticle = particlesList[i]

        currentParticle.advance()
        currentParticle.addVelocity(GRAVITY_X, GRAVITY_Y)

        if (currentParticle.isDead()):
            del particlesList[i]


def renderLoop(particlesList, color):
    # context = document.getElementById("canvas").getContext("2d")
    # context.clearRect( 0,0,WINDOW_X_SIZE,WINDOW_Y_SIZE )
    particle_img = np.zeros([720, 1280, 3], dtype=np.uint8)

    for i in range(0, len(particlesList)):
        particle_img = particlesList[i].draw(particle_img, color)

    return particle_img