import numpy as np
import cv2
import telnetlib
import imutils
import random
from particles import *
import array
HOST = "192.168.0.9"


kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))

videoSourceIndex = 0
cam = cv2.VideoCapture(cv2.CAP_DSHOW + 0)
# fgbg = cv2.bgsegm.createBackgroundSubtractorMOG(120)
fgbg = cv2.bgsegm.createBackgroundSubtractorGMG(30)
cam = cv2.VideoCapture(cv2.CAP_DSHOW + videoSourceIndex)
cam.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))
cam.set(3, 75)
cam.set(4, 32)
cam.set(cv2.CAP_PROP_AUTOFOCUS, 0)  # turn the autofocus off

color1 = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
color2 = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
gradient_start = [random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)]
gradient_end = [random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)]

contour_gradient_start = [random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)]
contour_gradient_end = [random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)]
count = 0
count2 = 0

VELOCITY_X_BASE = 0
VELOCITY_X_VARIANCE = 3
VELOCITY_Y_BASE = 0
VELOCITY_Y_VARIANCE = 3

#ser = serial.Serial('COM6', 115200)  # Establish the connection on a specific port
tn = telnetlib.Telnet(HOST)

stripCnt = 32  # Number of strips around the circumference of the sphere
ledCnt = 75
framerate = 25.0


def bytes2bits(ledData, layout):
    debugConvert = False

    colCnt, rowCnt, depth = ledData.shape
    linesPerPin = rowCnt // 8
    pixel = np.zeros((8, colCnt * linesPerPin, 3), 'uint8')
    bitBuffer = np.zeros((8 * (colCnt * linesPerPin) * 3), 'uint8')

    if False:
        print('bytes2bits ledData', ledData.shape)
        print(ledData[0:8, 0])
        print()

    for i in range(8):
        for y in range(linesPerPin):
            if ((y % 2) == (0 if layout else 1)):
                # even numbered rows are left to right
                if debugConvert:
                    print('>' + str((i * linesPerPin) + y), '', end='')
                pixel[i, y * colCnt:(y + 1) * colCnt] = ledData[0:ledCnt, i * linesPerPin + y]
            else:
                # odd numbered rows are right to left
                if debugConvert:
                    print('<' + str((i * linesPerPin) + y), '', end='')
                pixel[i, y * colCnt:(y + 1) * colCnt] = ledData[colCnt::-1, i * linesPerPin + y]

            if False:
                print('bytes2bits pixels', pixel.shape)
                print(pixel[0, 0:8])
                print()

        if debugConvert:
            print()

    if False:
        print('bytes2bits pixels', pixel.shape)
        print(pixel[0, 0:8, :])
        print()

    byteCnt = 0
    for led in range(colCnt * linesPerPin):
        for color in range(3):
            for bit in range(8):
                bitsOut = 0
                for pin in range(8):
                    if ((0x80 >> bit) & pixel[pin, led, color]):
                        bitsOut |= 1 << pin
                bitBuffer[byteCnt] = bitsOut
                byteCnt += 1

    if False:
        print('bytes2bits bitBuffer', bitBuffer.shape)
        for i in range(8):
            offset = i * 24

    return bitBuffer


particlesList = []
while (1):
    ret, frame = cam.read()

    resized = cv2.resize(frame, (75, 32))
    cv2.imshow("combo", resized)
    img = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)
    col1 = 0
    col2 = 1
    img.T[[col1, col2]] = img.T[[col2, col1]]
    img = np.transpose(img, (1, 0, 2))

    x = bytes2bits(img, True)
    serialData = []  # list(x)
    serialData.insert(0, 0xff)  # at 75% of the frame time
    serialData.insert(0, 0xff)  # request the frame sync pulse
    serialData.insert(0, ord('*'))  # first Teensy is the frame sync master

    tn.write(b''.join([str(i) for i in serialData]))
    tn.write(x)

    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()