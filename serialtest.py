import numpy as np
import serial
import cv2

ser = serial.Serial('COM6', 115200) # Establish the connection on a specific port

stripCnt = 32           # Number of strips around the circumference of the sphere
ledCnt = 75
framerate = 25.0

def getIfromRGB(rgb):
    red = rgb[0]
    green = rgb[1]
    blue = rgb[2]
    #print(red, green, blue)
    RGBint = (green<<16) + (red<<8) + blue
    #RGBint = (red<<16) + (green<<8) + blue
    return RGBint


def bytes2bits(ledData, layout):
    debugConvert = False

    colCnt, rowCnt, depth = ledData.shape
    linesPerPin = rowCnt // 8
    pixel = np.zeros((8, colCnt * linesPerPin, 3), 'uint8')
    bitBuffer = np.zeros((8 * (colCnt * linesPerPin) * 3), 'uint8')

    if False:
        print('bytes2bits ledData', ledData.shape)
        print(ledData[0:8, 0])
        print()

    for i in range(8):
        for y in range(linesPerPin):
            # print(i, y, y*colCnt)
            # print(i*linesPerPin+y,ledData[0, i*linesPerPin+y])
            if ((y % 2) == (0 if layout else 1)):
                # even numbered rows are left to right
                if debugConvert:
                    print('>' + str((i * linesPerPin) + y), '', end='')
                # pixel[i, y*colCnt:(y+1)*colCnt] = ledData[:, i*linesPerPin+y]
                pixel[i, y * colCnt:(y + 1) * colCnt] = ledData[0:ledCnt, i * linesPerPin + y]
            else:
                # odd numbered rows are right to left
                if debugConvert:
                    print('<' + str((i * linesPerPin) + y), '', end='')
                # pixel[i, y*colCnt:(y+1)*colCnt] = ledData[colCnt::-1, i*linesPerPin+y]
                pixel[i, y * colCnt:(y + 1) * colCnt] = ledData[colCnt::-1, i * linesPerPin + y]

            if False:
                print('bytes2bits pixels', pixel.shape)
                print(pixel[0, 0:8])
                print()

        if debugConvert:
            print()

    if False:
        print('bytes2bits pixels', pixel.shape)
        print(pixel[0, 0:8, :])
        print()

    byteCnt = 0
    for led in range(colCnt * linesPerPin):
        for color in range(3):
            for bit in range(8):
                bitsOut = 0
                for pin in range(8):
                    # print(led,color,bit,':',pixel[pin,led,color])
                    if ((0x80 >> bit) & pixel[pin, led, color]):
                        bitsOut |= 1 << pin
                # print(byteCnt,led,color,bit,'{0:02X}'.format(bitsOut))
                bitBuffer[byteCnt] = bitsOut
                byteCnt += 1

    if False:
        print('bytes2bits bitBuffer', bitBuffer.shape)
        for i in range(8):
            offset = i * 24
            print(bitBuffer[offset:offset + 8])
            print(bitBuffer[offset + 8:offset + 16])
            print(bitBuffer[offset + 16:offset + 24])
            print()

    #    for i in range(8*(colCnt*linesPerPin)*3):
    #        if (16 < (i%24)):
    #            bitBuffer[i] = 0xff
    #        else:
    #            bitBuffer[i] = 0x00

    return bitBuffer

def image2data(image, data=[37,97,215], layout=0):
    offset = 3;
    linesPerPin = 4
    pixel = []

    for y in range(0, linesPerPin):
        if ((y & 1) == layout):
        # even numbered rows are left to right
            xbegin = 0
            xend = 75
            xinc = 1
        else:
        # odd numbered rows are right to left
            xbegin = 75 - 1
            xend = -1
            xinc = -1

        for x in range(xbegin, xend, xinc):
            for i in range(0,8):
                # fetch 8 pixels from the image, 1 for each pin

                pixel.append(getIfromRGB(image[x + (y + linesPerPin * i) * 75]))
                #print(pixel[i])
                #print(img.shape)
                #pixel[i] = colorWiring(pixel[i]);
            # print(x, y)
        # convert 8 pixels to 24 bytes
        #for (mask = 0x800000; mask != 0; mask >>= 1) {
        mask = 0x800000
        while mask != 0:
            b = 0
            for i in range(0,8):
                if ((pixel[i] & mask) != 0):
                    #b |= (1 << i)
                    b = b | (1 << i)
            mask >>= 1
            data.append(b)
    return data








#ba2 = '*'.encode()
#for x in ba2:
cap = cv2.VideoCapture(0)
def run():

while True:
    ret, frame = cap.read()
    img = cv2.resize(frame, (75, 32))
    # Convert from BRG to RGB
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # Convert to GRB
    col1 = 0
    col2 = 1
    img.T[[col1, col2]] = img.T[[col2, col1]]

    # Convert to column-major
    img = np.transpose(img, (1, 0, 2))

    x = bytes2bits(img, True)
    serialData = []  # list(x)
    serialData.insert(0, 0xff)  # at 75% of the frame time
    serialData.insert(0, 0xff)  # request the frame sync pulse
    serialData.insert(0, ord('*'))  # first Teensy is the frame sync master
    print('len:', len(serialData))
    ser.write(serialData)
    ser.write(x)


