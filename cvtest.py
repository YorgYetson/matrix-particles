import numpy as np
import cv2
import serial
import imutils
import random
from particles import *

#cam = cv2.VideoCapture(r"C:\Users\Bull God\Desktop\moire.mp4")


kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))

#ser = serial.Serial('/dev/tty.usbmodem1d11', 9600) # Establish the connection on a specific port

videoSourceIndex = 0
cam = cv2.VideoCapture(cv2.CAP_DSHOW + 0)
#fgbg = cv2.bgsegm.createBackgroundSubtractorMOG(120)
fgbg = cv2.bgsegm.createBackgroundSubtractorGMG(30)
cam = cv2.VideoCapture(cv2.CAP_DSHOW + videoSourceIndex)
cam.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))
cam.set(3,1280)
cam.set(4,720)
cam.set(cv2.CAP_PROP_AUTOFOCUS, 0) # turn the autofocus off


color1 = (random.randint(0,255),random.randint(0,255),random.randint(0,255))
color2 = (random.randint(0,255),random.randint(0,255),random.randint(0,255))
gradient_start = [random.randint(0,255), random.randint(0,255), random.randint(0,255)]
gradient_end = [random.randint(0,255), random.randint(0,255), random.randint(0,255)]

contour_gradient_start = [random.randint(0,255), random.randint(0,255), random.randint(0,255)]
contour_gradient_end = [random.randint(0,255), random.randint(0,255), random.randint(0,255)]
count = 0
count2 = 0



VELOCITY_X_BASE = 0
VELOCITY_X_VARIANCE = 3
VELOCITY_Y_BASE = 0
VELOCITY_Y_VARIANCE = 3


ser = serial.Serial('COM6', 115200) # Establish the connection on a specific port

stripCnt = 32           # Number of strips around the circumference of the sphere
ledCnt = 75
framerate = 25.0

def getIfromRGB(rgb):
    red = rgb[0]
    green = rgb[1]
    blue = rgb[2]
    #print(red, green, blue)
    RGBint = (green<<16) + (red<<8) + blue
    #RGBint = (red<<16) + (green<<8) + blue
    return RGBint


def bytes2bits(ledData, layout):
    debugConvert = False

    colCnt, rowCnt, depth = ledData.shape
    linesPerPin = rowCnt // 8
    pixel = np.zeros((8, colCnt * linesPerPin, 3), 'uint8')
    bitBuffer = np.zeros((8 * (colCnt * linesPerPin) * 3), 'uint8')

    if False:
        print('bytes2bits ledData', ledData.shape)
        print(ledData[0:8, 0])
        print()

    for i in range(8):
        for y in range(linesPerPin):
            # print(i, y, y*colCnt)
            # print(i*linesPerPin+y,ledData[0, i*linesPerPin+y])
            if ((y % 2) == (0 if layout else 1)):
                # even numbered rows are left to right
                if debugConvert:
                    print('>' + str((i * linesPerPin) + y), '', end='')
                # pixel[i, y*colCnt:(y+1)*colCnt] = ledData[:, i*linesPerPin+y]
                pixel[i, y * colCnt:(y + 1) * colCnt] = ledData[0:ledCnt, i * linesPerPin + y]
            else:
                # odd numbered rows are right to left
                if debugConvert:
                    print('<' + str((i * linesPerPin) + y), '', end='')
                # pixel[i, y*colCnt:(y+1)*colCnt] = ledData[colCnt::-1, i*linesPerPin+y]
                pixel[i, y * colCnt:(y + 1) * colCnt] = ledData[colCnt::-1, i * linesPerPin + y]

            if False:
                print('bytes2bits pixels', pixel.shape)
                print(pixel[0, 0:8])
                print()

        if debugConvert:
            print()

    if False:
        print('bytes2bits pixels', pixel.shape)
        print(pixel[0, 0:8, :])
        print()

    byteCnt = 0
    for led in range(colCnt * linesPerPin):
        for color in range(3):
            for bit in range(8):
                bitsOut = 0
                for pin in range(8):
                    # print(led,color,bit,':',pixel[pin,led,color])
                    if ((0x80 >> bit) & pixel[pin, led, color]):
                        bitsOut |= 1 << pin
                # print(byteCnt,led,color,bit,'{0:02X}'.format(bitsOut))
                bitBuffer[byteCnt] = bitsOut
                byteCnt += 1

    if False:
        print('bytes2bits bitBuffer', bitBuffer.shape)
        for i in range(8):
            offset = i * 24
            print(bitBuffer[offset:offset + 8])
            print(bitBuffer[offset + 8:offset + 16])
            print(bitBuffer[offset + 16:offset + 24])
            print()

    #    for i in range(8*(colCnt*linesPerPin)*3):
    #        if (16 < (i%24)):
    #            bitBuffer[i] = 0xff
    #        else:
    #            bitBuffer[i] = 0x00

    return bitBuffer

def image2data(image, data=[37,97,215], layout=0):
    offset = 3;
    linesPerPin = 4
    pixel = []

    for y in range(0, linesPerPin):
        if ((y & 1) == layout):
        # even numbered rows are left to right
            xbegin = 0
            xend = 75
            xinc = 1
        else:
        # odd numbered rows are right to left
            xbegin = 75 - 1
            xend = -1
            xinc = -1

        for x in range(xbegin, xend, xinc):
            for i in range(0,8):
                # fetch 8 pixels from the image, 1 for each pin

                pixel.append(getIfromRGB(image[x + (y + linesPerPin * i) * 75]))
                #print(pixel[i])
                #print(img.shape)
                #pixel[i] = colorWiring(pixel[i]);
            # print(x, y)
        # convert 8 pixels to 24 bytes
        #for (mask = 0x800000; mask != 0; mask >>= 1) {
        mask = 0x800000
        while mask != 0:
            b = 0
            for i in range(0,8):
                if ((pixel[i] & mask) != 0):
                    #b |= (1 << i)
                    b = b | (1 << i)
            mask >>= 1
            data.append(b)
    return data



particlesList = []
while(1):
    ret, frame = cam.read()

    fgmask = fgbg.apply(frame)

    fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)
    img = np.zeros([720, 1280, 3], dtype=np.uint8)
    img2 = np.zeros([720, 1280, 3], dtype=np.uint8)
    color1 = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
    color2 = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

    image, contours, hier = cv2.findContours(fgmask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    gradient = (count,0,255-count)
    for i in range(1,3):
        if gradient_start[i] > gradient_end[i]:
            gradient_start[i] = gradient_start[i] - 15
        if gradient_start[i] < gradient_end[i]:
            gradient_start[i] = gradient_start[i] + 15
        if contour_gradient_start[i] > contour_gradient_end[i]:
            contour_gradient_start[i] = contour_gradient_start[i] - 15
        if contour_gradient_start[i] < contour_gradient_end[i]:
            contour_gradient_start[i] = contour_gradient_start[i] + 15
    if count == 30:
        #gradient_start = [random.randint(0,255), random.randint(0,255), random.randint(0,255)]
        gradient_end = [random.randint(0,255), random.randint(0,255), random.randint(0,255)]
        count=0
    if count2 == 30:
        contour_gradient_end = [random.randint(0,255), random.randint(0,255), random.randint(0,255)]
        count2 = 0
    count +=1
    count2 +=1

    img[:, 0:1280] = contour_gradient_start

    newcons = []
    for c in contours:
        if c.shape[0] > 800:
            newcons.append(c)

            # get the bounding rect
            x, y, w, h = cv2.boundingRect(c)
            # draw a green rectangle to visualize the bounding rect
            #cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

            # get the min area rect
            rect = cv2.minAreaRect(c)
            box = cv2.boxPoints(rect)
            # convert all coordinates floating point values to int
            box = np.int0(box)
            # draw a red 'nghien' rectangle
            #cv2.drawContours(frame, [box], 0, contour_gradient_start)

            # finally, get the min enclosing circle
            (x, y), radius = cv2.minEnclosingCircle(c)
            # convert all values to int
            center = (int(x), int(y))
            radius = int(radius)
            # and draw the circle in blue
            #frame = cv2.circle(frame, center, radius, (255, 0, 0), 2)
            SPAWN_RATE = 1
            for i in range(0, int(round(c.shape[0]/100))):
                #print(int(round(c.shape[0]/100)))
                offsetX = (random.randint(0, 10) * VELOCITY_X_VARIANCE) - (VELOCITY_X_VARIANCE / 2)
                offsetY = (random.randint(0, 10) * VELOCITY_Y_VARIANCE) - (VELOCITY_Y_VARIANCE / 2)
                #particlesList.append(Particle(int(x), int(y-100), VELOCITY_X_BASE + offsetX, VELOCITY_Y_BASE + offsetY))
                particlesList.append(Particle(int(x), int(y-100), VELOCITY_X_BASE + 0, VELOCITY_Y_BASE + 0))

    pimg = runMainLogic(particlesList, contour_gradient_start)

    cv2.drawContours(img, newcons, -1, gradient_start, cv2.FILLED)
    #cv2.imshow('res', frame)
    #cv2.imshow("colorcontours", img)
    #cv2.imshow("particles", pimg)
    combo = cv2.add(pimg, frame)

    resized = cv2.resize(combo, (75, 32))
    img = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)
    col1 = 0
    col2 = 1
    img.T[[col1, col2]] = img.T[[col2, col1]]
    img = np.transpose(img, (1, 0, 2))

    x = bytes2bits(img, True)
    serialData = []  # list(x)
    serialData.insert(0, 0xff)  # at 75% of the frame time
    serialData.insert(0, 0xff)  # request the frame sync pulse
    serialData.insert(0, ord('*'))  # first Teensy is the frame sync master
    #print('len:', len(serialData))
    ser.write(serialData)
    ser.write(x)


    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()